package client.constants;

/**
 * Created on 8/9/17.
 * This class ENUM dedicated to keep commands and descriptions for them. Those use for running tool
 * @author riakivets
 */
public enum CommandConstants {
    COUNT_UNIQUE_WORDS("countUniqueWords","Counts unique words in the file");

    private final String value;
    private final String description;

    CommandConstants(final String value, final String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
