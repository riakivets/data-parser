package client.constants;

/**
 *  Created on 8/9/17.
 *  This class ENUM dedicated to keep arguments and descriptions for them. Those use for running tool
 * @author riakivets
 */
public enum ArgumentsConstants {
    TEXT_FILE("<textFile>","The path to a file containing text data"),
    NUM_WORDS_OUTPUT("<numWordsOutput>","integer number N which indicates the amount of words to output");

    private final String value;
    private final String description;

    ArgumentsConstants(final String value, final String description) {
        this.value = value;
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

}
