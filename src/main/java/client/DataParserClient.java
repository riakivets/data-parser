package client;

import client.constants.ArgumentsConstants;
import client.constants.CommandConstants;
import com.google.common.base.Preconditions;
import org.apache.commons.io.IOUtils;
import org.docopt.Docopt;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This class it client to run tool for parsing textFile by regExp
 * Created on 8/9/17.
 *
 * @author riakivets
 */
public class DataParserClient {
    private static final InputStream DOCOPT_SCRIPT = DataParserClient.class.getClassLoader().
            getResourceAsStream("data-parser.script");
    private static final String REG_EXP_TO_PARSE = "[,.\\s]";

    public static void main(String[] programArguments) throws IOException {
        final StringWriter stringWriter = new StringWriter();
        IOUtils.copy(DOCOPT_SCRIPT, stringWriter, StandardCharsets.UTF_8);
        final Map<String, Object> docoptMap = new Docopt(stringWriter.toString()).withVersion("Data Parser tool").
                parse(programArguments);

        final Boolean countUniqueWords = (Boolean) docoptMap.get(CommandConstants.COUNT_UNIQUE_WORDS.getValue());
        if (countUniqueWords) {
            final String textDataFileName = docoptMap.get(ArgumentsConstants.TEXT_FILE.getValue()).toString();
            final List<String> words = readFileAndParseLinesToWords(textDataFileName, REG_EXP_TO_PARSE);
            final Map<String, ArrayList<String>> collect = words.stream().collect(Collectors.
                    groupingBy(String::toString, Collectors.toCollection(ArrayList::new)));

            final String linesOutputString = docoptMap.get(ArgumentsConstants.NUM_WORDS_OUTPUT.getValue()).toString();
            final Integer linesOutputInteger = Integer.parseInt(linesOutputString);
            Map<String, ArrayList<String>> orderedAlphabetically = collect.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                            (oldValue, newValue) -> oldValue, LinkedHashMap::new));
            if (linesOutputInteger>=collect.size()) {
                orderedAlphabetically.forEach((key, value) -> System.out.println(key + "=" + value.size()));
            } else {
                orderedAlphabetically.entrySet().stream().limit(linesOutputInteger).
                        forEach(entry -> System.out.println(entry.getKey() + "=" + entry.getValue().size()));
            }
        }
    }

    /**
     * Method ot read file, parse each line by regexp
     * and put result into list of string a.k.a. words
     * @param filePath - full path to text file
     * @param regExp - regExp for file to apply
     * @return - list of words
     * @throws IOException - if during processing we have some error
     */
    private static List<String> readFileAndParseLinesToWords(final String filePath, final String regExp) throws IOException {
        Preconditions.checkNotNull(filePath);
        List<String> lines = Files.readAllLines(Paths.get(filePath));
        return lines.stream().flatMap(line -> Arrays.stream(line.split(regExp))).
                filter(element->!element.trim().isEmpty()).map(String::toLowerCase).collect(Collectors.toList());
    }
}